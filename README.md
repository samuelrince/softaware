# SoftAWARE

This is a fork from the following project: [sdialliance/softawere-tools](https://gitlab.com/softawere-hackathon/softawere/)


## Usage

### Runner

**Start runner:**

```bash
make start_runner
```

Or alternatively, `source .env && docker compose -f docker-compose-runner.yml up -d`

**Stop runner:**

```bash
make stop_runner
```

Or alternatively, `docker compose -f docker-compose-runner.yml down`

### Dashboard

**Start dashboard:**

```bash
make start_dashboard
```

Or alternatively, `docker compose -f docker-compose-dashboard.yml up -d`

**Stop dashboard:**

```bash
make stop_dashboard
```

Or alternatively, `docker compose -f docker-compose-dashboard.yml down`

**Renew SSL certificate:**

```bash
docker compose -f docker-compose-dashboard.yml run --rm certbot renew
```

## Installation

### Runner

**Requirements:**

* docker and docker compose installed and up-to-date
* nvidia-driver and cuda-toolkits installed (for GPU install): *if the command `nvidia-smi` shows the GPU and CUDA version this should be good.*
* [nvidia-docker](https://docs.docker.com/config/containers/resource_constraints/#access-an-nvidia-gpu) setup: *if the command `docker run -it --rm --gpus all ubuntu nvidia-smi` shows the same output as above this should be good.*

**Install:**

Clone the project:

```bash
git clone https://gitlab.com/samuelrince/softaware.git
```

Add GitLab runner registration token in `./gitlab-runner/config/config.toml`:

```toml
...
[[runners]]
	...
	token = "CHANGEME"
	...
```

Start SoftAWARE Runner:

```bash
make start_runner
```

Or alternatively, `source .env && docker compose -f docker-compose-runner.yml up -d`

Then register the GitLab runner in the organisation: [Register a new group runner](https://gitlab.com/groups/green-ai-hackathon-2023-centralesupelec/-/runners/new)

Finally test the runner by manually launching the test CI job: [Run pipeline](https://gitlab.com/green-ai-hackathon-2023-centralesupelec/core/softaware-test/-/pipelines/new)


### Dashboard

Create SSL certificate:

```bash
docker compose -f docker-compose-dashboard.yml run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ -d greenaihack.org
```

Create `Impact score` formula:

TBD

Create `Score` formula:

TBD
