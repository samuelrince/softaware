SHELL := /bin/bash

start_dashboard:
	docker compose -f docker-compose-dashboard.yml up -d

stop_dashboard:
	docker compose -f docker-compose-dashboard.yml down

start_runner:
	source .env && docker compose -f docker-compose-runner.yml up -d

stop_runner:
	docker compose -f docker-compose-runner.yml down