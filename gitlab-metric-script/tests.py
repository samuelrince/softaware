import os
import contextlib
from datetime import datetime, timedelta

import requests
from prometheus_api_client import PrometheusConnect
from sty import fg, bg, ef


DOCKER_IP_GATEWAY = os.getenv('DOCKER_IP_GATEWAY', '172.17.0.1')

BOAGENT_ENDPOINT = f'http://{DOCKER_IP_GATEWAY}:8000'
PROMETHEUS_ENDPOINT = f'http://{DOCKER_IP_GATEWAY}:9090'
PUSHGATEWAY_ENDPOINT = f'{DOCKER_IP_GATEWAY}:9091'


def main():
    with test_runner('boagent'):
        test_boagent()

    with test_runner('prometheus'):
        test_prometheus()

    with test_runner('pushgateway'):
        test_pushgateway()

    with test_runner('host power'):
        test_query_host_power()

    with test_runner('gpu power'):
        test_query_gpu_power()


@contextlib.contextmanager
def test_runner(name: str):
    print(ef.bold + fg.blue + f'* Running test for `{name}`' + fg.rs + ef.rs)
    try:
        yield
        print(fg.green + 'TEST PASSED' + fg.rs)
    except Exception as e:
        print(bg.red + 'TEST FAILED' + bg.rs)
        print(e)


def test_boagent():
    url = "{}/query".format(BOAGENT_ENDPOINT)
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    if "start_time" in data:
        return
    print(f'JSON response is not valid.\n{data}')


def test_prometheus():
    url = '{}/-/healthy'.format(PROMETHEUS_ENDPOINT)
    response = requests.get(url)
    response.raise_for_status()
    if 'healthy' in response.text.lower():
        return
    print(f'Not healthy, response: {response.text}')


def test_pushgateway():
    url = 'http://{}/-/healthy'.format(PUSHGATEWAY_ENDPOINT)
    response = requests.get(url)
    response.raise_for_status()
    if response.text == "OK":
        return
    print(f'Not healthy, response: {response.text}')


def test_query_host_power():
    stopped_at = datetime.now()
    started_at = stopped_at - timedelta(seconds=60)
    prom = PrometheusConnect(url=PROMETHEUS_ENDPOINT, disable_ssl=True)
    metric = prom.get_metric_aggregation(
        'scaph_host_power_microwatts',
        operations=['average'],
        start_time=started_at,
        end_time=stopped_at,
        step='1'
    )
    avg_power = metric['average']
    print('Average power (RAPL):', avg_power)


def test_query_gpu_power():
    stopped_at = datetime.now()
    started_at = stopped_at - timedelta(seconds=60)
    prom = PrometheusConnect(url=PROMETHEUS_ENDPOINT, disable_ssl=True)
    metric = prom.get_metric_aggregation(
        'nvidia_smi_power_draw_watts',
        operations=['average'],
        start_time=started_at,
        end_time=stopped_at,
        step='1'
    )
    avg_power = metric['average']
    print('Average power (GPU):', avg_power)


if __name__ == '__main__':
    main()
