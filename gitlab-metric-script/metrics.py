import os
import time
from datetime import datetime

import requests
import pandas as pd
from prometheus_api_client import PrometheusConnect, MetricRangeDataFrame
from sklearn.metrics import accuracy_score
from sty import fg, bg, ef

# GPU embodied impacts
GPU_EMBODIED_GWP_IMPACT = 150
GPU_EMBODIED_ADP_IMPACT = 5.09e-3
GPU_EMBODIED_PE_IMPACT = 1828

# Conversions
LIFETIME_IN_YEARS = 5
SECONDS_IN_ONE_YEAR = 60 * 60 * 24 * 365
MICROWATTS_TO_WATTS = 1 / 1_000_000

# Endpoints
DOCKER_IP_GATEWAY = os.getenv('DOCKER_IP_GATEWAY', '172.17.0.1')
PROMETHEUS_ENDPOINT = f'http://{DOCKER_IP_GATEWAY}:9090'
BOAVIZTAPI_ENDPOINT = f'http://api.boavizta.org'

AIRTABLE_URL = "https://api.airtable.com/v0/appnAZfjJgWw5Alhm/tbluTIDtNmLDGYIOG"

DEBUG = bool(os.getenv('SOFTAWARE_DEBUG', False))
MAX_RETRY = 3
RETRY_BACKOFF = 5
PREDICTION_CSV = 'prediction.csv'


def main():
    ci_job_started_at = int(os.getenv("CI_JOB_EVAL_STARTED_AT"))
    ci_job_stopped_at = int(os.getenv("CI_JOB_EVAL_ENDED_AT"))
    impacts = compute_impacts(ci_job_started_at, ci_job_stopped_at)
    accuracy = compute_accuracy('prediction.csv')
    push_record(impacts, accuracy, ci_job_started_at, ci_job_stopped_at)
    print_summary(impacts, accuracy)


def compute_impacts(started_at, stopped_at) -> dict:
    duration = stopped_at - started_at
    avg_host_power = pull_avg_host_power_metrics_from_prom(started_at, stopped_at)
    impacts = boaviztapi_request(avg_host_power, duration)
    impacts = add_gpu_impacts(impacts, started_at, stopped_at)
    return impacts


def boaviztapi_request(avg_host_power_in_watts: float, duration_in_seconds: float) -> dict:
    data = {
        "model": {
            "type": "blade"
        },
        "configuration": {
            "cpu": {"units": 1, "die_size": 296},
            "ram": [{"units": 8, "capacity": 16}],
            "disk": [{"units": 1, "type": "ssd", "capacity": 931}],
            "power_supply": {"units": 1}
        },
        "usage": {
            "avg_power": avg_host_power_in_watts,
            "usage_location": "FRA",
            "hours_life_time": 35040
        }
    }
    duration = duration_in_seconds / 3600
    url = BOAVIZTAPI_ENDPOINT \
        + f'/v1/server/?verbose=true&duration={duration}&archetype=platform_compute_high&criteria=gwp&criteria=adp&criteria=pe'
    attempts = 1
    while attempts < MAX_RETRY:
        attempts += 1
        try:
            response = requests.post(url, json=data)
            response.raise_for_status()
            return response.json()
        except requests.exceptions.RequestException:
            print(f'Failed to query boaviztapi endpoint, retrying in {RETRY_BACKOFF} seconds')
            time.sleep(RETRY_BACKOFF)

    # Last attempt
    response = requests.post(url, json=data)
    if DEBUG and not response.ok:
        print('DEBUG NOCODB RESPONSE:')
        print(response.content)
    response.raise_for_status()
    return response.json()


def add_gpu_impacts(impacts, started_at, stopped_at):
    avg_gpu_power = pull_avg_gpu_power_metrics_from_prom(started_at, stopped_at)
    if avg_gpu_power >= 5:
        job_duration_in_seconds = stopped_at - started_at
        gpu_energy = (avg_gpu_power / 1000) * (job_duration_in_seconds / 3600)
        life_time_usage_ratio = job_duration_in_seconds / (LIFETIME_IN_YEARS * SECONDS_IN_ONE_YEAR)

        impacts['verbose']['avg_power']['value'] += avg_gpu_power
        impacts['impacts']['gwp']['embedded']['value'] += GPU_EMBODIED_GWP_IMPACT * life_time_usage_ratio
        impacts['impacts']['adp']['embedded']['value'] += GPU_EMBODIED_ADP_IMPACT * life_time_usage_ratio
        impacts['impacts']['pe']['embedded']['value'] += GPU_EMBODIED_PE_IMPACT * life_time_usage_ratio
        impacts['impacts']['gwp']['use']['value'] += gpu_energy * impacts['verbose']['gwp_factor']['value']
        impacts['impacts']['adp']['use']['value'] += gpu_energy * impacts['verbose']['adp_factor']['value']
        impacts['impacts']['pe']['use']['value'] += gpu_energy * impacts['verbose']['pe_factor']['value']
    return impacts


def pull_avg_gpu_power_metrics_from_prom(started_at, stopped_at) -> float:
    started_at = datetime.fromtimestamp(int(started_at))
    stopped_at = datetime.fromtimestamp(int(stopped_at))
    prom = PrometheusConnect(url=PROMETHEUS_ENDPOINT, disable_ssl=True)
    metric = prom.get_metric_aggregation(
        'nvidia_smi_power_draw_watts',
        operations=['average'],
        start_time=started_at,
        end_time=stopped_at,
        step='1'
    )
    avg_power = metric['average']
    return avg_power


def pull_avg_host_power_metrics_from_prom(started_at, stopped_at) -> float:
    started_at = datetime.fromtimestamp(int(started_at))
    stopped_at = datetime.fromtimestamp(int(stopped_at))
    prom = PrometheusConnect(url=PROMETHEUS_ENDPOINT, disable_ssl=True)
    metric = prom.get_metric_aggregation(
        'scaph_host_power_microwatts',
        operations=['average'],
        start_time=started_at,
        end_time=stopped_at,
        step='1'
    )
    avg_power = metric['average']
    return avg_power * MICROWATTS_TO_WATTS


def compute_accuracy(prediction_csv: str) -> float:
    pred_df = pd.read_csv(prediction_csv).sort_values(by='id')
    test_df = pd.read_csv(os.environ.get('TEST_DATASET_LABEL_URL')).sort_values(by='id')
    y_pred = pred_df['label'].to_numpy()
    y_true = test_df['label'].to_numpy()
    return accuracy_score(y_true, y_pred)


def push_record(
        impacts: dict,
        accuracy: float,
        started_at: float,
        stopped_at: float
):
    headers = {"Authorization": f"Bearer {os.environ['AIRTABLE_TOKEN']}"}
    team = os.getenv("CI_PROJECT_NAME")
    job_id = os.getenv("CI_JOB_ID")
    records_data = {
        "records": [
            {
                "fields": {
                    "ID": f"{team}-{job_id}",
                    "Team": team,
                    "Submission date": str(datetime.now()),
                    "Job ID": job_id,
                    "Job duration": stopped_at - started_at,
                    "Job embodied impact GWP": impacts["impacts"]["gwp"]["embedded"]["value"],
                    "Job operational impact GWP": impacts["impacts"]["gwp"]["use"]["value"],
                    "Job total impact GWP": impacts["impacts"]["gwp"]["embedded"]["value"] \
                                            + impacts["impacts"]["gwp"]["use"]["value"],
                    "Model accuracy": accuracy,
                }
            }
        ]
    }
    attempts = 1
    while attempts < MAX_RETRY:
        attempts += 1
        try:
            response = requests.post(AIRTABLE_URL, headers=headers, json=records_data)
            response.raise_for_status()
            return response.json()
        except requests.exceptions.RequestException:
            print(f'Failed to push record to Airtable, retrying in {RETRY_BACKOFF} seconds')
            time.sleep(RETRY_BACKOFF)

    # Last attempt
    response = requests.post(AIRTABLE_URL, headers=headers, json=records_data)
    if DEBUG and not response.ok:
        print("===DEBUG AIRTABLE===")
        print("Record data:")
        print(records_data)
        print("Response content:")
        print(response.content)
    response.raise_for_status()


def print_summary(impacts, accuracy_score):
    print(ef.bold)
    print("{}{}Submission results:{}".format(bg.da_red, fg.white, bg.rs))
    print("📊 {}Model Accuracy\n\t {:.3f}".format(fg.white, accuracy_score))

    print("⚡️ {}Electricity Consumption = {}Average Power {}* {}Job Duration".format(
        fg.white, fg.grey, fg.white, fg.grey)
    )
    print(
        "\t {}{:.2e} ({}) = {}{:.2e} {}* {}{:.2e}".format(
            fg.white,
            (impacts['verbose']['avg_power']['value'] / 1000) * impacts['verbose']['duration']['value'],
            'kWh',
            fg.grey,
            (impacts['verbose']['avg_power']['value'] / 1000),
            fg.white,
            fg.grey,
            impacts['verbose']['duration']['value'])
    )

    print(
        "🌍 {}Global Warming Potential = {}Operational GWP {}+ {}Embdedded GWP".format(
            fg.white, fg.grey, fg.white, fg.grey)
    )
    print(
        "\t {}{:.2e} ({}) = {}{:.2e} {}+ {}{:.2e}".format(
            fg.white,
            impacts['impacts']['gwp']['use']['value'] + impacts['impacts']['gwp']['embedded']['value'],
            impacts['impacts']['gwp']['unit'],
            fg.grey,
            impacts['impacts']['gwp']['use']['value'],
            fg.white,
            fg.grey,
            impacts['impacts']['gwp']['embedded']['value'])
    )

    print(
        "🪨 {}Abiotic Resources Depletion = {}Operational ADP {}+ {}Embdedded ADP".format(
            fg.white, fg.grey, fg.white, fg.grey)
    )
    print(
        "\t {}{:.2e} ({}) = {}{:.2e} {}+ {}{:.2e}".format(
            fg.white,
            impacts['impacts']['adp']['use']['value'] + impacts['impacts']['adp']['embedded']['value'],
            impacts['impacts']['adp']['unit'],
            fg.grey,
            impacts['impacts']['adp']['use']['value'],
            fg.white,
            fg.grey,
            impacts['impacts']['adp']['embedded']['value'])
    )

    print(
        "🛢️ {}Primary Energy Consumption = {}Operational PE {}+ {}Embdedded PE".format(
            fg.white, fg.grey, fg.white, fg.grey)
    )
    print(
        "\t {}{:.2e} ({}) = {}{:.2e} {}+ {}{:.2e}".format(
            fg.white,
            impacts['impacts']['pe']['use']['value'] + impacts['impacts']['pe']['embedded']['value'],
            impacts['impacts']['pe']['unit'],
            fg.grey,
            impacts['impacts']['pe']['use']['value'],
            fg.white,
            fg.grey,
            impacts['impacts']['pe']['embedded']['value'])
    )

    print()
    print("🏆 {}Check your submission on the leaderboard: {}".format(fg.white, "http://leaderboard.greenaihack.org"))


if __name__ == '__main__':
    main()
