#!/bin/bash

gitlab_runner=/usr/bin/gitlab-runner

${gitlab_runner} register -n -u ${GITL_RUNN_REGISTER_URL} -r "${GITL_RUNN_REGISTER_TOKEN}" --executor docker --docker-image ${GITL_RUNN_DOCKER_IMAGE}

if [ $? -gt 0 ]; then
  echo "WARNING: failed to register gitlab runner on ${GITL_RUNN_REGISTER_URL}"
fi

${gitlab_runner} run --user=root --working-directory=/home/gitlab-runner
