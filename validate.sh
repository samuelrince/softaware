#!/bin/bash

source .env

curl -s http://localhost:${PROM_LISTEN_PORT} >> /dev/null
if [ $? -gt 0 ]; then
  echo "Prometheus is not listening"
fi
curl -s http://localhost:${PROM_PUSH_LISTEN_PORT} >> /dev/null
if [ $? -gt 0 ]; then
  echo "Prometheus push gateway is not listening"
fi
curl -s http://localhost:${GITL_RUNN_EXPORTER_LISTEN_PORT}/metrics >> /dev/null
if [ $? -gt 0 ]; then
  echo "Gitlab Runner exporter for prometheus is not listening"
fi
curl -s http://localhost:${BOAG_LISTEN_PORT} >> /dev/null
if [ $? -gt 0 ]; then
  echo "Boagent is not listening"
fi
curl -s http://localhost:${BOAV_LISTEN_PORT} >> /dev/null
if [ $? -gt 0 ]; then
  echo "BoaviztAPI is not listening"
fi
curl -s http://localhost:${STATS_TCP_PORT} >> /dev/null
if [ $? -gt 0 ]; then
  echo "Container stats is not listening"
fi
curl -s http://localhost:${SCAPH_LISTEN_PORT}/metrics >> /dev/null
if [ $? -gt 0 ]; then
  echo "Scaphandre's prometheus exporter is not listening"
fi

